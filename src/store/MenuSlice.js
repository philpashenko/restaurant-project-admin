import {createSlice} from "@reduxjs/toolkit";

const initialState = {menu: []};

const menuSlice = createSlice({
    name: 'menuSlice',
    initialState,
    reducers: {
        addCategory(state, action) {
            state.menu = [ ...state.menu, { name: action.payload, subcategories: [] } ]
        },
        removeCategory(state, action) {
            state.menu = state.menu.filter(cat => cat.name !== action.payload);
        },
        addSubCategory(state, action) {
            let updatedCat = state.menu.findIndex(cat => cat.name === action.payload.catName);
            state.menu[updatedCat].subcategories.push({name: action.payload.newSubCat, dishes: []});
        },
        removeSubCategory(state, action) {
            let updatedCat = state.menu.findIndex(cat => cat.name === action.payload.catName);
            state.menu[updatedCat].subcategories = state.menu[updatedCat].subcategories.filter(subCat => subCat.name !== action.payload.subCatName);
        },
        addDish(state, action) {
            let updatedCat = state.menu.findIndex(cat => cat.name === action.payload.catName);
            let updatedSubCat = state.menu[updatedCat].subcategories.findIndex(cat => cat.name === action.payload.subCatName);
            state.menu[updatedCat].subcategories[updatedSubCat].dishes.push(action.payload.dish);
        },
        removeDish(state, action) {
            let updatedCat = state.menu.findIndex(cat => cat.name === action.payload.catName);
            let updatedSubCat = state.menu[updatedCat].subcategories.findIndex(cat => cat.name === action.payload.subCatName);
            state.menu[updatedCat].subcategories[updatedSubCat].dishes.filter(dish => dish.name !== action.payload.dishName);
        }
    }
})

export const menuActions = menuSlice.actions;

export default menuSlice;