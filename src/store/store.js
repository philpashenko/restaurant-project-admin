import {configureStore} from "@reduxjs/toolkit";
import menuSliceReducer from "./MenuSlice";

const store = configureStore({
    reducer: {menu: menuSliceReducer.reducer}
})

export default store;