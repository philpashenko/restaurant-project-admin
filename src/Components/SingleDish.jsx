import React, {useState} from "react";
import {Button, Form} from "react-bootstrap";
import {menuActions} from "../store/MenuSlice";
import {useDispatch} from "react-redux";

const styles = {
    dishWrap: {
        border: '1px solid #333',
        borderRadius: '4px',
        padding: '16px'
    },
    nameRow: {
        display: 'flex',
        width: '100%'
    }

}

const SingleDish = (props) => {

    const dispatch = useDispatch();

    const [editable, setEditable] = useState(props.editable)

    const [name, setName] = useState(
        () => {
            if (props.newDish === true) {
                return ''
            } else {
                return props.name;
            }
        }
    );

    const [price, setPrice] = useState(
        () => {
            if (props.newDish === true) {
                return ''
            } else {
                return props.price;
            }
        }
    );

    const [desc, setDesc] = useState(
        () => {
            if (props.newDish === true) {
                return ''
            } else {
                return props.desc;
            }
        }
    );

    const changeNameHandler = (event) => {
        setName(event.target.value);
    };

    const changePriceHandler = (event) => {
        setPrice(event.target.value);
    };

    const changeDescHandler = (event) => {
        setDesc(event.target.value);
    }

    const changeEditableHandler = () => {
        setEditable(!editable);
    }

    const addDishHandler = () => {
        const dish = {
            name: name,
            price: price,
            desc: desc
        }
        if (name !== '' && price !== '' && desc !== ''){
            dispatch(menuActions.addDish({
            dish, subCatName: props.subCatName, catName: props.catName }));
            props.resetAddDish();
        } else {
            window.alert('Ви не вказали назву категорії. Будь ласка вкажіть назву.');
        }
    }


    return(
        <div style={styles.dishWrap}>
            <div
                style={styles.nameRow}
            >
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Назва страви</Form.Label>
                    <Form.Control
                        disabled={!editable}
                        type="text"
                        placeholder="Ваша смачна страва"
                        value={name}
                        onChange={changeNameHandler}
                    />
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Ціна (uah)</Form.Label>
                    <Form.Control
                        disabled={!editable}
                        type="number"
                        step='0.1'
                        placeholder="24.5"
                        value={price}
                        onChange={changePriceHandler}
                    />
                </Form.Group>
                {
                    (!editable && !props.newDish)
                    ?
                    <div>
                        <Button
                            onClick={changeEditableHandler}
                        >
                            Редагувати
                        </Button>
                        <Button>
                            Видалити
                        </Button>
                    </div>
                    :
                    <></>
                }

            </div>

            <Form.Group className="mb-3">
                <Form.Label className='text-left'>Опис страви</Form.Label>
                <Form.Control
                    disabled={!editable}
                    type="text"
                    placeholder="Опис вашої смачної страви"
                    value={desc}
                    onChange={changeDescHandler}
                />
            </Form.Group>
            {
                editable
                ?
                <div>
                    <Button
                        onClick={addDishHandler}
                    >
                        Зберегти страву
                    </Button>
                    <Button
                        onClick={props.resetAddDish}
                    >
                        Скасувати
                    </Button>
                </div>
                :
                <></>
            }


        </div>
    );
}

export default SingleDish;