import React, { useState } from "react";
import {Button, Form} from 'react-bootstrap';
import {useDispatch, useSelector} from "react-redux";
import {menuActions} from "../store/MenuSlice";
import SingleCategory from "./SingleCategory";

const MenuCategories = () => {

    const [addCat, setAddCat] = useState(false);

    const [newCatName, setNewCatName] = useState('');

    const dispatch = useDispatch();

    const menu = useSelector(state => state.menu.menu);

    const changeAddCat = () => {
        setAddCat(!addCat);
    }
    const resetNewCatAdd = () => {
        setAddCat(!addCat);
        setNewCatName('');
    }

    const handleNewCatInputChange = (event) => {
        setNewCatName(event.target.value);
    }

    const addCategoryHandler = () => {
        if (newCatName !== ''){
            dispatch(menuActions.addCategory(newCatName));
            setNewCatName('');
            changeAddCat();
        } else {
            window.alert('Ви не вказали назву категорії. Будь ласка вкажіть назву.');
        }
    }

    return(
        <div>
            {menu.map((category, index) =>
                <SingleCategory
                    key={index}
                    catName={category.name}
                    subCats={category.subcategories}
                />
            )}

            <div>

                {
                    addCat
                        ?
                        <div>
                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                <Form.Label>Назва категорії</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Категорія"
                                    value={newCatName}
                                    onChange={handleNewCatInputChange}
                                />
                            </Form.Group>
                            <Button
                                onClick={addCategoryHandler}
                            >
                                Зберегти категорію
                            </Button>
                            <Button
                                onClick={resetNewCatAdd}
                            >
                                Скасувати
                            </Button>
                        </div>
                        :
                        <Button
                            onClick={changeAddCat}
                        >
                            Додати категорію
                        </Button>
                }

            </div>

        </div>
    );
};

export default MenuCategories;