import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import MenuCategories from "./MenuCategories";

const MenuEdit = () => {


    return(
        <Container>
            <Row>
                <Col className='my-4'>
                    <MenuCategories/>
                </Col>
            </Row>
        </Container>
    );
};

export default MenuEdit;